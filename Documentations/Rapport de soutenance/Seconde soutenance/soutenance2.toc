\select@language {french}
\contentsline {chapter}{\relax \fontsize {14.4}{18}\selectfont \bf Introduction}{2}
\contentsline {chapter}{\numberline {1}Rappels sur le cahier des charges}{3}
\contentsline {section}{\numberline {1.1}Le groupe Roubiconx}{3}
\contentsline {section}{\numberline {1.2}Le projet Enigma}{3}
\contentsline {section}{\numberline {1.3}Partage des t\^aches}{4}
\contentsline {section}{\numberline {1.4}Objectifs et d\'elais des t\^aches}{4}
\contentsline {chapter}{\numberline {2}Rappels de la premi\`ere soutenance}{5}
\contentsline {section}{\numberline {2.1}Narration et sc\'enario}{5}
\contentsline {section}{\numberline {2.2}D\'eveloppement des IA}{5}
\contentsline {section}{\numberline {2.3}Scripts joueurs}{6}
\contentsline {section}{\numberline {2.4}Level Design}{7}
\contentsline {section}{\numberline {2.5}Site Web}{7}
\contentsline {chapter}{\numberline {3}Avancement du projet}{9}
\contentsline {section}{\numberline {3.1}D\'eveloppement des IA}{9}
\contentsline {section}{\numberline {3.2}Scripts joueurs}{10}
\contentsline {section}{\numberline {3.3}Level Design}{11}
\contentsline {section}{\numberline {3.4}Site Web}{12}
\contentsline {section}{\numberline {3.5}R\'esum\'e de l'avancement}{13}
\contentsline {chapter}{\numberline {4}Prochains objectifs}{14}
\contentsline {section}{\numberline {4.1}Visuel \& Sonore}{14}
\contentsline {section}{\numberline {4.2}Gameplay}{14}
\contentsline {section}{\numberline {4.3}Syst\`eme de jeu}{14}
\contentsline {section}{\numberline {4.4}Autres}{14}
\contentsline {chapter}{\numberline {5}Conclusions personnelles}{15}
\contentsline {section}{\numberline {5.1}Nicolas \bsc {Acart}}{15}
\contentsline {section}{\numberline {5.2}Guillaume \bsc {Saladin}}{15}
\contentsline {section}{\numberline {5.3}Antonin \bsc {Ginet}}{15}
\contentsline {chapter}{\relax \fontsize {14.4}{18}\selectfont \bf Conclusion}{16}
