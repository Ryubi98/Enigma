\select@language {french}
\contentsline {chapter}{\relax \fontsize {14.4}{18}\selectfont \bf Introduction}{2}
\contentsline {chapter}{\numberline {1}Rappels sur le cahier des charges}{3}
\contentsline {section}{\numberline {1.1}Le groupe Roubiconx}{3}
\contentsline {section}{\numberline {1.2}Le projet Enigma}{3}
\contentsline {section}{\numberline {1.3}Partage des t\^aches}{4}
\contentsline {section}{\numberline {1.4}Objectifs et d\'elais des t\^aches}{4}
\contentsline {chapter}{\numberline {2}Avancement de notre projet}{5}
\contentsline {section}{\numberline {2.1}Narration et sc\'enario}{5}
\contentsline {section}{\numberline {2.2}D\'eveloppement des IA}{5}
\contentsline {section}{\numberline {2.3}Scripts joueurs}{6}
\contentsline {section}{\numberline {2.4}Level Design}{7}
\contentsline {section}{\numberline {2.5}Site Web}{7}
\contentsline {section}{\numberline {2.6}R\'esum\'e de l'avancement}{8}
\contentsline {chapter}{\numberline {3}Prochains objectifs}{9}
\contentsline {section}{\numberline {3.1}Visuel \& Sonore}{9}
\contentsline {section}{\numberline {3.2}Gameplay}{9}
\contentsline {section}{\numberline {3.3}Syst\`eme de jeu}{9}
\contentsline {section}{\numberline {3.4}Autre}{9}
\contentsline {chapter}{\numberline {4}Conclusions personnelles}{10}
\contentsline {section}{\numberline {4.1}JeanJacquelin}{10}
\contentsline {section}{\numberline {4.2}GuiGui}{10}
\contentsline {section}{\numberline {4.3}Ryubi}{10}
\contentsline {chapter}{\relax \fontsize {14.4}{18}\selectfont \bf Conclusion}{11}
