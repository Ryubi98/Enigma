\select@language {french}
\contentsline {chapter}{\relax \fontsize {14.4}{18}\selectfont \bf Introduction}{2}
\contentsline {chapter}{\numberline {1}Pr\'esentation du groupe}{3}
\contentsline {section}{\numberline {1.1}Formation du groupe}{3}
\contentsline {section}{\numberline {1.2}Pr\'esentation des membres}{3}
\contentsline {subsection}{\numberline {1.2.1}Nicolas \bsc {Acart} - "Jean Jacquelin"}{3}
\contentsline {subsection}{\numberline {1.2.2}Antonin \bsc {Ginet} - "Ryubi"}{4}
\contentsline {subsection}{\numberline {1.2.3}Guillaume \bsc {Saladin} - "Guigui"}{4}
\contentsline {chapter}{\numberline {2}Pr\'esentation du projet}{5}
\contentsline {section}{\numberline {2.1}Inspiration}{5}
\contentsline {section}{\numberline {2.2}Objet d'\'etude}{6}
\contentsline {chapter}{\numberline {3}Moyens et comp\'etences}{7}
\contentsline {section}{\numberline {3.1}Mat\'eriel}{7}
\contentsline {subsection}{\numberline {3.1.1}Hardware}{7}
\contentsline {subsection}{\numberline {3.1.2}Software}{7}
\contentsline {section}{\numberline {3.2}Ce que le projet nous apporte :}{8}
\contentsline {subsection}{\numberline {3.2.1}Individuellement}{8}
\contentsline {subsection}{\numberline {3.2.2}En groupe}{8}
\contentsline {chapter}{\numberline {4}D\'ecoupage du projet}{9}
\contentsline {section}{\numberline {4.1}Les diff\'erentes t\^aches du projet}{9}
\contentsline {subsection}{\numberline {4.1.1}Narration}{9}
\contentsline {subsection}{\numberline {4.1.2}Interfaces}{9}
\contentsline {subsection}{\numberline {4.1.3}Intelligences Artificielle}{9}
\contentsline {subsection}{\numberline {4.1.4}Scripts joueurs}{9}
\contentsline {subsection}{\numberline {4.1.5}Level design}{10}
\contentsline {subsection}{\numberline {4.1.6}R\'eseau}{10}
\contentsline {subsection}{\numberline {4.1.7}Musique}{10}
\contentsline {subsection}{\numberline {4.1.8}Site web}{10}
\contentsline {subsection}{\numberline {4.1.9}Installation}{10}
\contentsline {section}{\numberline {4.2}Partage des t\^aches}{11}
\contentsline {section}{\numberline {4.3}Planning des t\^aches}{11}
\contentsline {chapter}{\relax \fontsize {14.4}{18}\selectfont \bf Conclusion}{12}
